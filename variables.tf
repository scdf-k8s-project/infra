variable "project_name" {
  type = string
}

variable "cred_file" {
  type = string
}

variable "cluster_name" {
  default = "av-k8s-cluster"
}

variable "region_name" {
  default = "europe-north1"
}

variable "location_name" {
  default = "europe-north1-b"
}

variable "count_vms" {
  default = "1"
}

variable "machine_type" {
  default = "e2-standard-4"
}

variable "k8s_master_version" {
  default = "1.18.12-gke.1200"
}

variable "gke_k8s_channel" {
  default = "RAPID"
}

variable "node_disk_size" {
  default = 30
}



variable "bucket_name" {
  description = "GCS Bucket name. Value should be unique ."
  type        = string
  default     = "ab-k8s-tf-bucket"
}
