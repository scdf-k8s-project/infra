terraform {
  required_version = ">= 0.12.26"

  backend "gcs" {
    bucket      = "av_terraform_bucket"
    credentials = "service_account.json"
    prefix      = "av-k8s"
  }

}

provider "google-beta" {
  version     = "~> 3.15.0"
  project     = var.project_name
  region      = var.region_name
  credentials = file(var.cred_file)
}

provider "google" {
  version     = "~> 3.15.0"
  project     = var.project_name
  region      = var.region_name
  credentials = file(var.cred_file)
}

resource "google_container_cluster" "av-k8s-cluster" {
  name               = var.cluster_name
  location           = var.location_name
  min_master_version = var.k8s_master_version
  logging_service    = "none"
  release_channel {
    channel = var.gke_k8s_channel
  }


  provider                 = google-beta
  remove_default_node_pool = true
  initial_node_count       = 1

  addons_config {
    istio_config {
      disabled = true
      auth     = "AUTH_NONE"
    }

  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

}

resource "google_container_node_pool" "av-k8s-nodes" {
  name       = "av-k8s-node-pool"
  location   = var.location_name
  cluster    = google_container_cluster.av-k8s-cluster.name
  node_count = var.count_vms

  node_config {
    preemptible  = true
    machine_type = var.machine_type
    disk_size_gb = var.node_disk_size

    metadata = {
      disable-legacy-endpoints = "true"
    }
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

resource "google_compute_address" "ip_address" {
  name = "ip-adress-for-nginx-ingress"
  depends_on = [
    google_container_node_pool.av-k8s-nodes
  ]
}
